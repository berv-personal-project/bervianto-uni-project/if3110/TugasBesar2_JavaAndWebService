<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%-- 
    Document   : index
    Created on : Nov 6, 2016, 3:02:22 PM
    Author     : raudi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML> 
<html> 	
    <head> 
	<title>Sign-In</title> 
	<link rel="stylesheet" type="text/css" href="css/main.css"> 
	<script type="text/javascript" src="js/validate.js"></script>
    </head> 
    <body id="body-color"> 
        <div>
            <h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
	</div>
	<div>
            <h2> Please login </h2>
	</div>
		
	<form method="GET" action="sign-in.php" onsubmit="return validateSignIn()"> 
            <label for="fname">Email or Username</label><br>
            <input type="text" id="username" name="username" ><br>
            <div id="notif-username" class="notif">
            </div>
            <label for="fname">Password </label><br>
            <input type="password" id="password" name="password" ><br>
            <div id="notif-password" class="notif">
            </div>
            <div class="submit-style">
                <input id="button" type="submit" name="login" value="LOGIN"> 
            </div>
	</form>
	<br><br><br>
	<div class="register"> 
            <b>Dont have an account yet? Register <a class="link-register" href="register.php">here</a></b>
	</div>		
    </body> 
</html>
