<%-- 
    Document   : catalog
    Created on : Nov 8, 2016, 10:28:46 PM
    Author     : raudi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>

<!DOCTYPE HTML>
<html>     
    <%-- start web service invocation --%>
    <%
	account.Accounts_Service serviceValid = new account.Accounts_Service();
	account.Accounts portValid = serviceValid.getAccountsPort();
	String token = null;
        Cookie cookie = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            cookie = cookies[i];
            if((cookie.getName()).compareTo("token") == 0 ){
               token = cookie.getValue();
               //cookie.setMaxAge(0);
            }
        }
        //java.lang.String token = "qwertyuiop";
	// TODO process result here
	account.AccountObject resultValid = portValid.validate(token);
	if (resultValid.getValidate() == 2) {   
        for (int i = 0; i < cookies.length; i++) {
        	cookie = cookies[i];
        	if((cookie.getName()).compareTo("token") == 0 ){
               token = cookie.getValue();
               cookie.setMaxAge(0);
           	}
        }
        Cookie cookieExpires = new Cookie("token",resultValid.getToken());
        response.addCookie(cookieExpires);
	} else if (resultValid.getValidate() == 3) {
       response.sendRedirect("http://localhost:8000/WebApp/index.jsp");
    }
    %>

	<head> 
		<title>Catalog</title>  
		<link rel="stylesheet" type="text/css" href="css/main.css">
		
		<script type="text/javascript" src="js/function.js"></script>
	</head> 
	<body id="body-color"> 
		<div>
			<h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
		</div>
		<div class="atas">
			Hi, <%out.println(resultValid.getUsername());%>!<br>
			<a href="logout.jsp" class="logout"><font size="1">logout</font></a>
		</div>
		<div class="navbar">
		<ul>
                    <li><a class="active" href="catalog.jsp">Catalog</a></li>
                    <li><a href="your_product.jsp">Your Products</a></li>
                    <li><a href="add_product.jsp">Add Products</a></li>
                    <li><a href="sales.jsp">Sales</a></li>
                    <li><a href="purchases.jsp">Purchases</a></li>
		</ul>
		<br><br>
		</div>
		<div>
			<h2> What are you going to buy today? </h2>
		</div>
            <form action="catalog.jsp" id="search" method="post">
                    <input type="text" name="search" id="search" class="searchfield" placeholder="Search catalog...">
                    <input class="search" type="submit" name="go" value="GO">
                    <div class="filter">
                            <div class="by">
                                    <span> by</span>
                            </div>
                            <div class="filtersearch">
                                    <input type="radio" id="filter" name="filter" value="product" checked/> product <br>
                                    <input type="radio" id="filter" name="filter" value="store"/> store
                             </div>
                             <br><br>
                    </div>
                </form>
		<%-- start web service invocation --%>
                <%
                    java.util.List<item.ItemObject> result = null;
                    String keyword = request.getParameter("search");
                    String filter = request.getParameter("filter");
                    item.Items_Service service = new item.Items_Service();
                    if (keyword == null) {
                        item.Items port = service.getItemsPort();
                        // TODO process result here
                        result = port.getAllItems();
                    } else if (keyword.equals("")) {
                        item.Items port = service.getItemsPort();
                        // TODO process result here
                        result = port.getAllItems();
                    } else {
                       if (filter.equals("product")) {
                            item.Items port = service.getItemsPort();
                            // TODO process result here
                            result = port.searchItemByName(keyword);
                        } else { 
                            item.Items port = service.getItemsPort();
	                    // TODO process result here
                            //out.println("masuk");
                            result = port.searchItemBySeller(keyword);
                        }
                    }
                    NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();
                    for(int i=0;i<result.size();i++) {
                
                %>
                <%-- end web service invocation --%>
                
		<div class="filter">
			
			<div class="product-view">
				<%-- start web service invocation --%>
                                <%
                                    account.Accounts_Service serviceAccount = new account.Accounts_Service();
                                    account.Accounts portUsername = serviceAccount.getAccountsPort();
                                     // TODO initialize WS operation arguments here
                                    int idSeller = result.get(i).getSeller();
                                    // TODO process result here
                                    java.lang.String resultUsername = portUsername.getUsername(idSeller);
                                    out.println(resultUsername);
                                %>
                                <%-- end web service invocation --%>
				<br>
				added this on <% out.print(result.get(i).getDate()); %>, at <% out.print(result.get(i).getTime()); %>
			</div>
			<div class="product-view">
				<div class="photo">
					<img src=<% out.print(result.get(i).getPhoto()); %> alt="Mountain View" width="100px" height="100px">
				</div>
				<div class="description">
					<b><% out.print(result.get(i).getName()); %></b> <br>
					<% out.print(defaultFormat.format(result.get(i).getPrice())); %><br>
					<font size="1"> <% out.print(result.get(i).getDescription()); %></font>
				</div>
                                    <div class="detail" id="detail<% out.print("id"); %>">
					
					<div>
						<font size="1"> <%
                                                                    item.Items portCountLikes = service.getItemsPort();
                                                                     // TODO initialize WS operation arguments here
                                                                    int accessTokenCountLikes = resultValid.getId();
                                                                    int idCountLikes = result.get(i).getId();
                                                                    // TODO process result here
                                                                    int resultCountLikes = portCountLikes.countLikes(accessTokenCountLikes, idCountLikes);

                                                                    out.print(resultCountLikes);
                                                                %> likes </font>
					</div>
					<div class="npurchase">
						<font size="1"> <% 
                                                                item.Items portCountPurchases = service.getItemsPort();
                                                                 // TODO initialize WS operation arguments here
                                                                int accessTokenCountPurchases = resultValid.getId();
                                                                int idCountPurchases = result.get(i).getId();
                                                                // TODO process result here
                                                                int resultCountPurchases = portCountPurchases.countPurchases(accessTokenCountPurchases, idCountPurchases);
                                                                out.print(resultCountPurchases); 
                                                                %> purchases </font><br>
					</div>
					<% 
                                            
                                            item.Items portCheck = service.getItemsPort();
                                             // TODO initialize WS operation arguments here
                                            int accessToken = resultValid.getId();
                                            int id = result.get(i).getId();
                                            // TODO process result here
                                            boolean check = portCheck.checkLike(accessToken, id);
                                            
                                            if (check == false) {
                                        %>
                                        <%-- end web service invocation --%>
                                                <div class="like-button">
                                                    <div style="display:inline-block;width:40%;">
                                                        <form method="post" id="like" action="LikeServlet">
                                                            <input id="accessToken" type="hidden" name="accessToken" value="<%= resultValid.getId() %>">
                                                            <input id="id" type="hidden" name="id" value="<%= result.get(i).getId() %>">
                                                            <input id="search" type="hidden" name="search" value="<%= request.getParameter("search") %>">
                                                            <input id="filter" type="hidden" name="filter" value="<%= request.getParameter("filter") %>">
                                                            <input type="submit" class="like" value="LIKE">	
                                                        </form>
                                                    </div>
                                                </div>
                                        <%  } else { %>
						<div class="like-button">
                                                    <div style="display:inline-block;width:40%;">
                                                        <form method="post" id="dislike" action="DislikeServlet">
                                                            <input id="accessToken" type="hidden" name="accessToken" value="<%= resultValid.getId() %>">
                                                            <input id="id" type="hidden" name="id" value="<%= result.get(i).getId() %>">
                                                            <input id="search" type="hidden" name="search" value="<%= request.getParameter("search") %>">
                                                            <input id="filter" type="hidden" name="filter" value="<%= request.getParameter("filter") %>">
                                                            <input type="submit" class="liked" value="LIKED">	
                                                        </form>
                                                    </div>
						</div>
                                        <%    }  %>
					
					<div class="buy-button">
                                            <div style="display:inline-block;width:40%;">
                                            <form method="post" id="purchaseProduct" action="confirm_purchase.jsp">
                                                <input id="accessToken" type="hidden" name="accessToken" value="<%= resultValid.getId() %>">
                                                <input id="id" type="hidden" name="id" value="<%= result.get(i).getId() %>">
                                                <input type="submit" class="buy" value="BUY">	
                                            </form>
                                        </div>	
					</div>
				</div>
			</div>
		</div>
                <% } %>
	</body> 
</html>
