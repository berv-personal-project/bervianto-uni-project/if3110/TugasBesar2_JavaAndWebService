<%-- 
    Document   : index
    Created on : Nov 6, 2016, 3:02:22 PM
    Author     : raudi
--%>

<%@page import="org.json.JSONException"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="javax.xml.ws.ProtocolException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.MalformedURLException"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML> 
<html> 	
    <% 
        account.Accounts_Service serviceValid = new account.Accounts_Service();
	account.Accounts portValid = serviceValid.getAccountsPort();
	String token = null;
        Cookie cookiecek = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            cookiecek = cookies[i];
            if((cookiecek.getName()).compareTo("token") == 0 ){
               token = cookiecek.getValue();
               //cookie.setMaxAge(0);
            }
        }
	//java.lang.String token = "qwertyuiop";
	// TODO process result here
	account.AccountObject resultValid = portValid.validate(token);
	if (resultValid.getValidate() == 1) {
            response.sendRedirect("http://localhost:8000/WebApp/catalog.jsp");
        }
        
        
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        
        //String username = "bebas";
        //String password = "bebas";
        
        String USER_AGENT = "Mozilla/5.0";
        
        String url = "http://localhost:8001/IdentityService/SignIn";
        URL obj = null;
        try {
            obj = new URL(url);
        } catch (MalformedURLException ex) {
        }
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
        } catch (IOException ex) {
            
        }

        try {
            //add reuqest header
            con.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            
        }
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "username="+username+"&password="+password+"&email="+email;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = null;
        
        try {
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
        } catch (IOException ex) {
        }
        
        StringBuffer responsebuff = new StringBuffer();
        try {
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                    responsebuff.append(inputLine);
            }
            in.close();
        } catch (IOException ex) {
        }
        
        JSONObject JSobjek = null;  
        JSobjek = new JSONObject(responsebuff.toString());

        //str = (String)JSobjek.get("id");
        int respId = (int)JSobjek.getInt("id");
        //if (!str.equals("null")) {
        //respId = Integer.parseInt(str);
        //}
        if (respId == 0) {
            //response.sendRedirect("http://localhost:8000/WebApp/index.jsp");
        } else {
            Cookie cookie = new Cookie("token",(String)JSobjek.getString("token"));
            response.addCookie(cookie);
            response.sendRedirect("http://localhost:8000/WebApp/catalog.jsp");
        }
    %>
    <head> 
	<title>Sign-In</title> 
	<link rel="stylesheet" type="text/css" href="css/main.css"> 
	<script type="text/javascript" src="js/validate.js"></script>
    </head> 
    <body id="body-color"> 
  
        <div>
            <h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
	</div>
	<div>
            <h2> Please login </h2>
	</div>
		
	<form method="POST" action="index.jsp"> 
            <label for="fname">Email or Username</label><br>
            <input type="text" id="username" name="username" ><br>
            <div id="notif-username" class="notif">
            </div>
            <label for="fname">Password </label><br>
            <input type="password" id="password" name="password" ><br>
            <div id="notif-password" class="notif">
            </div>
            <div class="submit-style">
                <input id="button" type="submit" name="login" value="LOGIN"> 
            </div>
	</form>
	<br><br><br>
	<div class="register"> 
            <b>Dont have an account yet? Register <a class="link-register" href="register.jsp">here</a></b>
	</div>		
    </body> 
</html>
