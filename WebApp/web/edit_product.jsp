<%-- 
    Document   : edit_product
    Created on : Nov 8, 2016, 10:52:14 PM
    Author     : raudi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 
<html> 
    <%-- start web service invocation --%>
    <%
	account.Accounts_Service serviceValid = new account.Accounts_Service();
	account.Accounts portValid = serviceValid.getAccountsPort();
	String token = null;
        Cookie cookie = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            cookie = cookies[i];
            if((cookie.getName( )).compareTo("token") == 0 ){
               token = cookie.getValue();
               //cookie.setMaxAge(0);
            }
        }
	//java.lang.String token = "qwertyuiop";
	// TODO process result here
	account.AccountObject resultValid = portValid.validate(token);
	if (resultValid.getValidate() == 2) {   
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if((cookie.getName()).compareTo("token") == 0 ){
                   token = cookie.getValue();
                   cookie.setMaxAge(0);
                }
            }
            Cookie cookieExpires = new Cookie("token",resultValid.getToken());
            response.addCookie(cookieExpires);
        } else if (resultValid.getValidate() == 3) {
            response.sendRedirect("http://localhost:8000/WebApp/index.jsp");
        }
    %>
	<head> 
		<title>Edit Product</title> 
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<script type="text/javascript" src="js/validate.js"></script>
	</head> 
	<body id="body-color"> 
		
 		<div>
 			<h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
 		</div>
		<div class="atas">
			Hi, <%out.println(resultValid.getUsername());%>!<br>
			<a href="logout.jsp" class="logout"><font size="1">logout</font></a>
		</div>
		<div class="navbar">
		<ul>
			<li><a href="catalog.jsp">Catalog</a></li>
 			<li><a href="your_product.jsp">Your Products</a></li>
 			<li><a href="add_product.jsp">Add Products</a></li>
 			<li><a href="sales.jsp">Sales</a></li>
			<li><a href="purchases.jsp">Purchases</a></li>
 		</ul>
		</div>
		<br>
		<div>
			<h2> Please update your product here </h2>
		</div>
                    <%-- start web service invocation --%>
                    <%
                        item.Items_Service service = new item.Items_Service();
                        item.Items port = service.getItemsPort();
                         // TODO initialize WS operation arguments here
                        String strId = request.getParameter("id");
                        int id = Integer.parseInt(strId);
                        // TODO process result here
                        item.ItemObject result = port.getItemByID(id);
                    
                    %>
                    <%-- end web service invocation --%>

		<form method="post" id="edit_product" action="EditProductServlet">
                    <input type="hidden" name="id" id="id" value="<%=result.getId()%>">
			<label for="product_name">Name</label><br>
                        <input type="text" name="name" id="name" value="<% out.print(result.getName()); %>"><br>
			<div id="notif-name" class="notif">
			</div>
			<label for="product_desc">Description (max 200 chars)</label><br>
			<textarea rows="5" cols="107" name="description" id="description"><% out.print(result.getDescription()); %></textarea><br>
			<div id="notif-description" class="notif">
			</div>
			<label for="product_price">Price (IDR)</label><br>
			<input type="text" name="price" id="price" value="<% out.print(result.getPrice()); %>"><br>
			<div id="notif-price" class="notif">
			</div>
			<label for="product_photo">Photo</label><br>
			<input type="file" accept="image/*" align="middle" name="photo" id="photo" disabled><br>
			<div class="notif" id="notif">
				 <p>* Sorry, you can't edit your product photo.</p>
			</div>
			<div class="submit-style">
				<input id="button" type="submit" name="edit_product" value="UPDATE"> 
				<input id="cancel" class="cancel" type="button" size="20" value="CANCEL" onclick="window.location.href='your_product.jsp'"> 
			</div>
		</form>	
	</body> 
</html>


