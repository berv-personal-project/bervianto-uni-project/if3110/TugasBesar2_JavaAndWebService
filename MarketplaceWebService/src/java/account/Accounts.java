/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account;


import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import org.json.*;

/**
 *
 * @author raudi
 */
@WebService(serviceName = "Accounts")
public class Accounts {

    /**
     * This is a sample web service operation
     * @param token
     * @return 
     */
    @WebMethod(operationName = "validate")
    public accountObject validate(@WebParam(name = "token") String token) {
        String USER_AGENT = "Mozilla/5.0";
        
        String url = "http://localhost:8001/IdentityService/ValidateToken";
        URL obj = null;
        try {
            obj = new URL(url);
        } catch (MalformedURLException ex) {
        }
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
        } catch (IOException ex) {
            
        }

        try {
            //add reuqest header
            con.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            
        }
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "token="+token;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = null;
        
        try {
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
        } catch (IOException ex) {
        }
        
        StringBuffer response = new StringBuffer();
        try {
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
            }
            in.close();
        } catch (IOException ex) {
        }
        System.out.println(response.toString());
        
        accountObject account = null;
        JSONObject JSobjek = null;  
        try {
            JSobjek = new JSONObject(response.toString());
            
            account = new accountObject((int)JSobjek.get("id"), 
                                    (String)JSobjek.get("fullname"),
                                    (String)JSobjek.get("username"),
                                    (String)JSobjek.get("address"),
                                    (String)JSobjek.get("postalcode"),
                                    (String)JSobjek.get("phonenumber"),
                                    (String)JSobjek.get("token"),
                                    (int)JSobjek.get("validate") );
        } catch (JSONException ex) {
        }
        return account;

    }
    
    /**
     * This is a sample web service operation
     * @param id
     * @return 
     */
    @WebMethod(operationName = "getUsername")
    public String getUsername(@WebParam(name = "id") int id) {
        String USER_AGENT = "Mozilla/5.0";
        
        String url = "http://localhost:8001/IdentityService/GetUsername";
        URL obj = null;
        try {
            obj = new URL(url);
        } catch (MalformedURLException ex) {
        }
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
        } catch (IOException ex) {
            
        }

        try {
            //add reuqest header
            con.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            
        }
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "id="+id;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = null;
        
        try {
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
        } catch (IOException ex) {
        }
        
        StringBuffer response = new StringBuffer();
        try {
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
            }
            in.close();
        } catch (IOException ex) {
        }
        System.out.println(response.toString());
        
        String uname = null;
        JSONObject JSobjek = null;  
        try {
            JSobjek = new JSONObject(response.toString());
            uname = (String)JSobjek.get("username");
        } catch (JSONException ex) {
        }
        return uname;
        
    }
}
