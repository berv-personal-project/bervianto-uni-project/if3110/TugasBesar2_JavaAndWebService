/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validate;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.security.SecureRandom;
import java.math.BigInteger;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author raudi
 */
@WebServlet(name = "Register", urlPatterns = {"/Register"})
public class Register extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String username = request.getParameter("username");
            String fullname = request.getParameter("fullname");
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            String address = request.getParameter("address");
            String postalcode = request.getParameter("postalcode");
            String phonenumber = request.getParameter("phonenumber");
            
            if (username.equals("null")) {
                JSONObject json = new JSONObject();
                    
                json.put("id",0);
                response.setContentType("application/json");
                response.getWriter().write(json.toString());
            }
            else {
                Class.forName("com.mysql.jdbc.Driver");
                try
                (   Connection conn = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/Identity?zeroDateTimeBehavior=convertToNull",
                        "root",
                        ""); 
                    Statement stmt = conn.createStatement()) {
                    
                    String sqlCekUname = "SELECT * FROM account WHERE username = ?";
                    String sqlCekEmail = "SELECT * FROM account WHERE email = ?";
                    PreparedStatement dbStatementUname = conn.prepareStatement(sqlCekUname);
                    dbStatementUname.setString(1, username);
                    ResultSet rsUname = dbStatementUname.executeQuery();

                    PreparedStatement dbStatementEmail = conn.prepareStatement(sqlCekEmail);
                    dbStatementEmail.setString(1, email);
                    ResultSet rsEmail = dbStatementEmail.executeQuery();
                    
                    if ((rsUname.next()) || (rsEmail.next())) {
                        JSONObject json = new JSONObject();
                    
                        json.put("id",0);
                        response.setContentType("application/json");
                        response.getWriter().write(json.toString());  
                    } else {
                        String sql;
                        sql = "INSERT INTO account(fullname,username,email,password,address,postalcode,phonenumber) VALUES(?,?,?,?,?,?,?)";
                        PreparedStatement dbStatement = conn.prepareStatement(sql,stmt.RETURN_GENERATED_KEYS);
                        dbStatement.setString(1, fullname);
                        dbStatement.setString(2, username);
                        dbStatement.setString(3, email);
                        dbStatement.setString(4, password);
                        dbStatement.setString(5, address);
                        dbStatement.setString(6, postalcode);
                        dbStatement.setString(7, phonenumber);

                        /* Get every data returned by SQL query */
                        int id = dbStatement.executeUpdate();
                        ResultSet rs = dbStatement.getGeneratedKeys();
                        //int id = 0;
                        if(rs.next())
                        {
                            id = rs.getInt(1);
                        }
                        /* Get every data returned by SQL query */
                        JSONObject json = new JSONObject();
                        boolean expires = false;
                        SecureRandom random = new SecureRandom();
                        String token = new BigInteger(130, random).toString(32);

                        String sqlCek;
                        boolean ulang = true;
                        PreparedStatement dbStatementCek;

                        while (ulang) {
                            token = new BigInteger(130, random).toString(32);
                            sqlCek = "SELECT * FROM token WHERE token = ?";
                            dbStatementCek = conn.prepareStatement(sqlCek);
                            dbStatementCek.setString(1, token);
                            ResultSet rsCek = dbStatementCek.executeQuery();
                            if (rsCek.next()) {
                                ulang = true;
                            } else {
                                ulang = false;
                            }
                        }

                        Date dNow = new Date();
                        SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd");
                        SimpleDateFormat timeFormat = new SimpleDateFormat ("HH:mm:ss");
                        String time_expired = timeFormat.format(dNow);
                        String date_expired = dateFormat.format(dNow);
                        String sqlInputToken;
                        sqlInputToken = "INSERT INTO token(id_account,token,date_expired,time_expired) VALUES(?,?,?,?)";
                        PreparedStatement dbStatementInputToken = conn.prepareStatement(sqlInputToken);
                        dbStatementInputToken.setInt(1, id);
                        dbStatementInputToken.setString(2, token);
                        dbStatementInputToken.setString(3, date_expired);
                        dbStatementInputToken.setString(4, time_expired);
                        int rsInputToken = dbStatementInputToken.executeUpdate();

                        json.put("id",id);
                        json.put("fullname",fullname);
                        json.put("username",username);
                        json.put("address",address);
                        json.put("postalcode",postalcode);
                        json.put("phonenumber",phonenumber);
                        json.put("token",token);
                        response.setContentType("application/json");
                        response.getWriter().write(json.toString());
                        stmt.close();
                        conn.close();
                    }
                } catch (SQLException | IOException ex) {
                    Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
