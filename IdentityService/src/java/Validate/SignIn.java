/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validate;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author raudi
 */
@WebServlet(name = "SignIn", urlPatterns = {"/SignIn"})
public class SignIn extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String username = request.getParameter("username");
            String email = request.getParameter("email");
            String password = request.getParameter("password");
           
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(SignIn.class.getName()).log(Level.SEVERE, null, ex);
            }
            try
            (   Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Identity?zeroDateTimeBehavior=convertToNull",
                    "root",
                    ""); 
                Statement stmt = conn.createStatement()) {
                String sql;
                sql = "SELECT * FROM account WHERE username = ? and password = ?";
                PreparedStatement dbStatement = conn.prepareStatement(sql);
                dbStatement.setString(1, username);
                dbStatement.setString(2, password);
                JSONObject json = new JSONObject();
                /* Get every data returned by SQL query */
                ResultSet rs = dbStatement.executeQuery();
                if (rs.next()) {
                    
                    SecureRandom random = new SecureRandom();
                    String token = new BigInteger(130, random).toString(32);

                    String sqlCek;
                    boolean ulang = true;
                    PreparedStatement dbStatementCek;

                    while (ulang) {
                        token = new BigInteger(130, random).toString(32);
                        sqlCek = "SELECT * FROM token WHERE token = ?";
                        dbStatementCek = conn.prepareStatement(sqlCek);
                        dbStatementCek.setString(1, token);
                        ResultSet rsCek = dbStatementCek.executeQuery();
                        if (rsCek.next()) {
                            ulang = true;
                        } else {
                            ulang = false;
                        }
                    }

                    Date dNow = new Date();
                    SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd");
                    SimpleDateFormat timeFormat = new SimpleDateFormat ("HH:mm:ss");
                    String time_expired = timeFormat.format(dNow);
                    String date_expired = dateFormat.format(dNow);
                    String sqlInputToken;
                    sqlInputToken = "INSERT INTO token(id_account,token,date_expired,time_expired) VALUES(?,?,?,?)";
                    PreparedStatement dbStatementInputToken = conn.prepareStatement(sqlInputToken);
                    dbStatementInputToken.setInt(1, rs.getInt("id"));
                    dbStatementInputToken.setString(2, token);
                    dbStatementInputToken.setString(3, date_expired);
                    dbStatementInputToken.setString(4, time_expired);
                    int rsInputToken = dbStatementInputToken.executeUpdate();
                    
                    json.put("id",rs.getInt("id"));
                    json.put("fullname",rs.getString("fullname"));
                    json.put("username",rs.getString("username"));
                    json.put("address",rs.getString("address"));
                    json.put("postalcode",rs.getString("postalcode"));
                    json.put("phonenumber",rs.getString("phonenumber"));
                    json.put("token",token);
                } else {
                    String sqlEmail;
                    sqlEmail = "SELECT * FROM account WHERE email = ? and password = ?";
                    PreparedStatement dbStatementEmail = conn.prepareStatement(sqlEmail,Statement.RETURN_GENERATED_KEYS);
                    dbStatementEmail.setString(1, email);
                    dbStatementEmail.setString(2, password);
                    ResultSet rsEmail = dbStatementEmail.executeQuery();
                    if (rsEmail.next()) {
                        SecureRandom random = new SecureRandom();
                        String token = new BigInteger(130, random).toString(32);

                        String sqlCek;
                        boolean ulang = true;
                        PreparedStatement dbStatementCek;

                        while (ulang) {
                            token = new BigInteger(130, random).toString(32);
                            sqlCek = "SELECT * FROM token WHERE token = ?";
                            dbStatementCek = conn.prepareStatement(sqlCek);
                            dbStatementCek.setString(1, token);
                            ResultSet rsCek = dbStatementCek.executeQuery();
                            if (rsCek.next()) {
                                ulang = true;
                            } else {
                                ulang = false;
                            }
                        }

                        Date dNow = new Date();
                        SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd");
                        SimpleDateFormat timeFormat = new SimpleDateFormat ("HH:mm:ss");
                        String time_expired = timeFormat.format(dNow);
                        String date_expired = dateFormat.format(dNow);
                        String sqlInputToken;
                        sqlInputToken = "INSERT INTO token(id_account,token,date_expired,time_expired) VALUES(?,?,?,?)";
                        PreparedStatement dbStatementInputToken = conn.prepareStatement(sqlInputToken);
                        dbStatementInputToken.setInt(1, rs.getInt("id"));
                        dbStatementInputToken.setString(2, token);
                        dbStatementInputToken.setString(3, date_expired);
                        dbStatementInputToken.setString(4, time_expired);
                        int rsInputToken = dbStatementInputToken.executeUpdate();
                        
                        json.put("id",rsEmail.getInt("id"));
                        json.put("fullname",rsEmail.getString("fullname"));
                        json.put("username",rsEmail.getString("username"));
                        json.put("address",rsEmail.getString("address"));
                        json.put("postalcode",rsEmail.getString("postalcode"));
                        json.put("phonenumber",rsEmail.getString("phonenumber"));
                        json.put("token",token);
                    } else {
                        json.put("id",0);
                        json.put("fullname","");
                        json.put("username","");
                        json.put("address","");
                        json.put("postalcode","");
                        json.put("phonenumber","");
                    }
                }
                /* Get every data returned by SQL query */
                
                response.setContentType("application/json");
                response.getWriter().write(json.toString());
                rs.close();
                stmt.close();
                conn.close();
            }
        } catch (SQLException | IOException ex) {
            
        }   
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
