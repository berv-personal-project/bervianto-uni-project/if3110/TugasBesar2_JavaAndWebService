<%-- 
    Document   : confirm_purchase
    Created on : Nov 8, 2016, 10:51:38 PM
    Author     : raudi
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<html> 
    <%-- start web service invocation --%>
    <%
	account.Accounts_Service serviceValid = new account.Accounts_Service();
	account.Accounts portValid = serviceValid.getAccountsPort();
	String token = null;
        Cookie cookie = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            cookie = cookies[i];
            if((cookie.getName( )).compareTo("token") == 0 ){
               token = cookie.getValue();
               //cookie.setMaxAge(0);
            }
        }
	//java.lang.String token = "qwertyuiop";
	// TODO process result here
        NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();
                    
	account.AccountObject resultValid = portValid.validate(token);
	if (resultValid.getValidate() == 2) {   
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if((cookie.getName()).compareTo("token") == 0 ){
                   token = cookie.getValue();
                   cookie.setMaxAge(0);
                }
            }
            Cookie cookieExpires = new Cookie("token",resultValid.getToken());
            response.addCookie(cookieExpires);
        } else if (resultValid.getValidate() == 3) {
            response.sendRedirect("http://localhost:8000/WebApp/index.jsp");
        }
    %>
	<head> 
		<title>Confirm Purchase</title> 
		<link rel="stylesheet" type="text/css" href="css/main.css"> 
		<script type="text/javascript" src="js/validate.js"></script>
		<script type="text/javascript" src="js/function.js"></script>
	</head> 
	<%-- start web service invocation --%>
        <%
            item.Items_Service service = new item.Items_Service();
            item.Items port = service.getItemsPort();
             // TODO initialize WS operation arguments here
            String strId = request.getParameter("id");
            int id = Integer.parseInt(strId);
            // TODO process result here
            item.ItemObject result = port.getItemByID(id);
            Date dNow = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat ("EEEE',' dd MMMM y");
            SimpleDateFormat timeFormat = new SimpleDateFormat ("HH.mm");
        %>
        <%-- end web service invocation --%>
	<body id="body-color" onload="countTotalPrice(<% out.print(result.getPrice()); %>)"> 
		
		<div>
			<h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
		</div>
		<div class="atas">
			Hi, <%out.println(resultValid.getUsername());%>!<br>
			<a href="logout.jsp" class="logout"><font size="1">logout</font></a>
		</div>
		<div class="navbar">
		<ul>
			<li><a href="catalog.jsp">Catalog</a></li>
			<li><a href="your_product.jsp">Your Products</a></li>
			<li><a href="add_product.jsp">Add Products</a></li>
			<li><a href="sales.jsp">Sales</a></li>
			<li><a href="purchases.jsp">Purchases</a></li>
		</ul>
		<br><br>
		</div>
		<div>
			<h2> Please confirm your purchase </h2>
		</div>
		    
                        
		<form method="post" id="confirm" action="ConfirmationPurchaseServlet" onsubmit="return validatePurchase();">
			<input id="idSeller" type="hidden" name="idSeller" value="<%=result.getSeller()%>">
                        <input id="idItem" type="hidden" name="idItem" value="<%=result.getId()%>">
                        <input id="idBuyer" type="hidden" name="idBuyer" value="<%=resultValid.getId()%>">
                    
                        <label>Product : <% out.println(result.getName()); %></label> <br>
			<input id="itemName" type="hidden" name="itemName" value="<%=result.getName()%>">
                        <label>Price : <div id="item-price" name="item-price" class="show"><% out.print(defaultFormat.format(result.getPrice())); %></div></label>  <br>
                        <input id="itemPrice" type="hidden" name="itemPrice" value="<%=result.getPrice()%>">
                        <label for="quantity">Quantity : 
			<input id="quantity" class="quantity" type="text" name="quantity" value="1" onchange="countTotalPrice(<% out.print(result.getPrice()); %>)"> pcs</label> <br>
			<label>Total Price : IDR<div id="total-price" class="show"></div></label> <br>
			<label> Delivery to : </label> <br>
			<br>
			
			<label for="consignee"> Consignee </label> <br>
			<input id="consignee" type="text" name="consignee" value="<%=resultValid.getFullname()%>" onchange="return validateFullName(this.value)"> <br>
			<div id="notif-consignee" class="notif">
			</div>
			
			<label for='address'> Full Address </label><br>
			<textarea id="fullAddress" name="fullAddress"><%=resultValid.getAddress()%></textarea><br>
			<div id="notif-fullAddress" class="notif">
			</div>
			
			<label for="postalcode"> Postal Code </label> <br>
			<input id="postalCode" type="text" name="postalCode" value="<%=resultValid.getPostalcode()%>" onchange="return validatePostalCode(this.value)"> <br>
			<div id="notif-postalCode" class="notif">
			</div>	
			
			<label for="phone"> Phone </label> <br>
			<input id="phoneNumber" type="text" name="phoneNumber" value="<%=resultValid.getPhonenumber()%>" onchange="return validatePhoneNumber(this.value)"> <br>
			<div id="notif-phoneNumber" class="notif">
			</div>
			
			<label for="creditcardnumber"> 12 Digits Credit Card Number </label> <br>
			<input id="ccNumber" type="text" name="ccNumber" onchange="return validateCCNumber()"> <br>
			<div id="notif-ccNumber" class="notif">
			</div>
			
			<label for="cardvalue"> 3 Digits Card Value </label> <br>
			<input id="verificationCode" type="text" name="verificationCode" onchange="return validateCCValue()"> <br>
			<div id="notif-verificationCode" class="notif">
			</div>
			<input type="hidden" id="date" name="date" value="<%=dateFormat.format(dNow) %>">
                        <input type="hidden" id="time" name="time" value="<%=timeFormat.format(dNow) %>">
            
			<div class="submit-style">
				<input id="submit" type="submit" size="20" name="confirm" value="CONFIRM"> 
				<input id="cancel" class="cancel" type="button" size="20" value="CANCEL" onclick="window.location.href='catalog.jsp'"> 
			</div>
		</form>	
	</body> 
        
</html>


