/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validate;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;


/**
 *
 * @author raudi
 */
@WebServlet(name = "ValidateToken", urlPatterns = {"/ValidateToken"})
public class ValidateToken extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            java.lang.String token = request.getParameter("token");
           
            Class.forName("com.mysql.jdbc.Driver");
            try
            (   Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Identity?zeroDateTimeBehavior=convertToNull",
                    "root",
                    ""); 
                Statement stmt = conn.createStatement()) {
                String sql;
                sql = "SELECT id,fullname,username,address,postalcode,phonenumber,time_expired,date_expired FROM account,token WHERE id = id_account and token = ?";
                PreparedStatement dbStatement = conn.prepareStatement(sql);
                dbStatement.setString(1, token);
                /* Get every data returned by SQL query */
                ResultSet rs = dbStatement.executeQuery();
                /* Get every data returned by SQL query */
                JSONObject json = new JSONObject();
                boolean expires = false;
                /* Get every data returned by SQL query */
                Date dateNow = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
                
                if(rs.next()){
                    String date = rs.getString("date_expired");
                    String time = rs.getString("time_expired");
                    String datetime = date + " " + time;
                    Date dateExpireStart = null;
                    try {
                        dateExpireStart = dateFormat.parse(datetime);
                    } catch (ParseException ex) {
                        Logger.getLogger(ValidateToken.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    long msDiff = dateNow.getTime() - dateExpireStart.getTime();
                    if (msDiff > 900000) {
                        expires = true;
                    }
                    if (expires) {
                        SecureRandom random = new SecureRandom();
                        String tokenExpires = new BigInteger(130, random).toString(32);

                        String sqlCek;
                        boolean ulang = true;
                        PreparedStatement dbStatementCek;

                        while (ulang) {
                            tokenExpires = new BigInteger(130, random).toString(32);
                            sqlCek = "SELECT * FROM token WHERE token = ?";
                            dbStatementCek = conn.prepareStatement(sqlCek);
                            dbStatementCek.setString(1, tokenExpires);
                            ResultSet rsCek = dbStatementCek.executeQuery();
                            if (rsCek.next()) {
                                ulang = true;
                            } else {
                                ulang = false;
                            }
                        }

                        Date dNow = new Date();
                        SimpleDateFormat dateFormatExpires = new SimpleDateFormat ("yyyy-MM-dd");
                        SimpleDateFormat timeFormatExpires = new SimpleDateFormat ("HH:mm:ss");
                        String time_expired = timeFormatExpires.format(dNow);
                        String date_expired = dateFormatExpires.format(dNow);
                        String sqlInputToken;
                        sqlInputToken = "INSERT INTO token(id_account,token,date_expired,time_expired) VALUES(?,?,?,?)";
                        PreparedStatement dbStatementInputToken = conn.prepareStatement(sqlInputToken);
                        dbStatementInputToken.setInt(1, rs.getInt("id"));
                        dbStatementInputToken.setString(2, tokenExpires);
                        dbStatementInputToken.setString(3, date_expired);
                        dbStatementInputToken.setString(4, time_expired);
                        int rsInputToken = dbStatementInputToken.executeUpdate();
                        
                        String sqlDelete;
                        sqlDelete = "DELETE FROM token WHERE token = ?";
                        PreparedStatement dbStatementDelete = conn.prepareStatement(sqlDelete,Statement.RETURN_GENERATED_KEYS);
                        dbStatementDelete.setString(1, token);

                        /* Get every data returned by SQL query */
                        int rsDelete = dbStatementDelete.executeUpdate();
                        
                        json.put("id",rs.getInt("id"));
                        json.put("fullname",rs.getString("fullname"));
                        json.put("username",rs.getString("username"));
                        json.put("address",rs.getString("address"));
                        json.put("postalcode",rs.getString("postalcode"));
                        json.put("phonenumber",rs.getString("phonenumber"));
                        json.put("token",tokenExpires);
                        json.put("validate",2);
                    } else {
                        json.put("id",rs.getInt("id"));
                        json.put("fullname",rs.getString("fullname"));
                        json.put("username",rs.getString("username"));
                        json.put("address",rs.getString("address"));
                        json.put("postalcode",rs.getString("postalcode"));
                        json.put("phonenumber",rs.getString("phonenumber"));
                        json.put("token","");
                        json.put("validate",1);
                    }
                } else {
                    json.put("id",0);
                    json.put("fullname","");
                    json.put("username","");
                    json.put("address","");
                    json.put("postalcode","");
                    json.put("phonenumber","");
                    json.put("token","");
                    json.put("validate",3);
                }
                
                response.setContentType("application/json");
                response.getWriter().write(json.toString());
                rs.close();
                stmt.close();
                conn.close();
            }
        } catch (ClassNotFoundException | SQLException ex) {
            
        }   
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
