# Java And Web Service

## Penjelasan

![Arsitektur Umum](arsitektur_umum.png)

### Basis Data

#### Basis Data Identitas

##### Tabel Account

| address | email | fullname | id   | password | phonenumber | postalcode | username |
| ------- | ----- | -------- | ---- | -------- | ----------- | ---------- | -------- |
|         |       |          |      |          |             |            |          |

##### Tabel Token

| date_expired | id_account | time_expired | token |
| ------------ | ---------- | ------------ | ----- |
|              |            |              |       |

#### Basis Data Market Place

##### Tabel Buys

| cc_number | consignee | date | full_adress | id   | id_buyer | id_item | id_seller | item_name | item_price | phone_number | photo | postal_code | quantity | time | verification_code |
| --------- | --------- | ---- | ----------- | ---- | -------- | ------- | --------- | --------- | ---------- | ------------ | ----- | ----------- | -------- | ---- | ----------------- |
|           |           |      |             |      |          |         |           |           |            |              |       |             |          |      |                   |

##### Tabel Item

| date | description | id   | name | photo | price | seller | time |
| ---- | ----------- | ---- | ---- | ----- | ----- | ------ | ---- |
|      |             |      |      |       |       |        |      |

##### Tabel Likes

| id   | id_account | id_item |
| ---- | ---------- | ------- |
|      |            |         |

### Konsep Shared Session dengan Menggunakan REST
Arsitektur REST merupakan abstraksi dari elemen arsiterkural di dalam sistem terdistribusi. REST mengabaikan detil dari komponen implementasi dan protokol sintaks untuk lebih fokus kepada peran dari setiap komponen, batasan dalam interaksi antar komponen, dan interpretasi dari data element. 

REST aplikasi web ini tidak lagi stateless. Hal ini dapat diwujudkan dengan cara memberikan variabel pengenal (dalam tugas ini token) di di dalam body dari request dalam JSON/XML. Server akan melakukan otentikasi sesi menggunakan token yang dipetakan ke identitas client. Hal ini membuat setiap request memunculkan token yang digunakan untuk melakukan pemeriksaan terhadap client. Token ini akan hilang ketika user melakukan logout atau expire.

### Pembangkitan Token dan Expire Time pada Sistem
Sistem membangkitkan token ketika client pertama kali melakukan request kepada server (register atau login). Token ini memberikan akses kepada client untuk melakukan request-request tambahan kepada server. Token baru dibangkitkan ketika client melakukan Login atau Register request kepada server. Token memiliki lifetime yang terbatas (expire time), setelah melewati batas waktu, token tidak dapat lagi digunakan untuk melakukan request kepada server. Client harus membuat Login atau Register request baru untuk mendapatkan token baru untuk mengakses server. 

### Kelebihan dan Kelemahan dari Arsitektur Aplikasi
Kelebihan:
- Lebih tidak rumit dalam debug karena aplikasi tidak menyelesaikan setiap tugas yang diberikan. Ada aplikasi tertentu yang melakukan tugas tertentu.
- Semua server tidak perlu memeriksa setiap request setiap saat (meringankan server).
- Jadi lebih mudah memanage server (mudah mendeteksi bagian server yg mana yg trouble karena dipecah).

Kekurangan:
- Sulit melakukan debug jika terdapat bug pada suatu bagian modul yang menggunakan semua server.

## Pembagian Tugas

### REST

1. Generate Token : 13514087
2. Validasi Token : 13514087
3. Register : 13514081
4. Sig In : 13514081
5. Logout : 13514047
6. GetUsernameby ID : 13514047


### SOAP

1. ​Web Service item : 13514087
2. Web Service buys : 13514081
3. Web Service account : 13514047

### Web App

1. Halaman Login : 13514087
2. Halaman Register : 13514087
3. Add Product : 13514047
4. ​Edit Product : 13514047
5. Your Product : 13514047
6. Catalog : 13514081
7. Sales : 13514081
8. Purchases : 13514081
9. Confirmation Purchase : 13514087

## About

Kelompok iDuar :

1. Bervianto Leo P - 13514047
2. Ahmad Faiq Rahman - 13514081
3. Praditya Raudi A - 13514087

